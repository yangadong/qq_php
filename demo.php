<?php
session_start(); //此示例中要使用session
require_once('config.php');
require_once('qq.php');

$qq_t=isset($_SESSION['qq_t'])?$_SESSION['qq_t']:'';

//检查是否已登录
if($qq_t!=''){
	$qq=new qqPHP($qq_k, $qq_s, $qq_t);
	$qq_oid=$qq->get_openid();
	$openid=$qq_oid['openid']; //获取登录用户open id

	//获取登录用户信息
	$result=$qq->get_user_info($openid);
	var_dump($result);

	/**
	//发布分享
	$title='开源中国'; //分享页面标题
	$url='http://www.oschina.net/'; //分享页面网址
	$site=''; //QQ应用名称
	$fromurl='';  //QQ应用网址
	$result=$qq->add_share($openid, $title, $url, $site, $fromurl);
	var_dump($result);
	**/

	/**
	//其他功能请根据官方文档自行添加
	//示例：根据openid获取用户信息
	$result=$qq->api('user/get_user_info', array('openid'=>$openid), 'GET');
	var_dump($result);
	**/

}else{
	//生成登录链接
	$qq=new qqPHP($qq_k, $qq_s);
	$login_url=$qq->login_url($callback_url, $scope);
	echo '<a href="',$login_url,'">点击进入授权页面</a>';
}
